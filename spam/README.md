
[![](https://www.r-pkg.org/badges/version-ago/spam)](https://cran.r-project.org/package=spam)
[![](https://cranlogs.r-pkg.org/badges/spam)](https://cran.r-project.org/package=spam)
[![](https://cranlogs.r-pkg.org/badges/grand-total/spam)](https://cranlogs.r-pkg.org/badges/grand-total=spam)
[![Lifecycle:stable](https://img.shields.io/badge/lifecycle-stable-green.svg)](https://www.tidyverse.org/lifecycle/#stable)


A set of functions for sparse matrix algebra.
Differences with other sparse matrix packages are:

 *  we only support (essentially) one sparse matrix format,
 *  based on transparent and simple structure(s),
 *  tailored for MCMC calculations within G(M)RF.
 *  and it is fast and scalable (with the extension package spam64).

<img src="man/figures/germanyadjacency.png" align="center" />


<!---
```{r setup, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.width = 7, fig.height = 7,
  fig.align = "center"
)
library(spam)
```


```{r sparsematrix, echo=FALSE, warning=FALSE, fig.cap = '\\label{fig:sparsematrix}'}
graphics.off()
png("man/figures/germanyadjacency.png")
path <- system.file("demodata/germany.adjacency", package="spam")
W <- spam::adjacency.landkreis(path)
par(pty = 's', las = 1)
spam::display.spam(W, cex = 2)
dev.off()
```
-->



